package edu.eco.resilence.npd.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.eco.resilence.npd.bo.IEcoResilenceBO;
import edu.eco.resilence.npd.model.AppDetail;
import edu.eco.resilence.npd.model.Login;
import edu.eco.resilence.npd.model.Person;
import edu.eco.resilence.npd.model.ResultMessage;
import edu.eco.resilence.npd.model.Detail;
import edu.eco.resilence.npd.model.District;
import edu.eco.resilence.npd.model.Section;
import edu.eco.resilence.npd.model.State;
import edu.eco.resilence.npd.model.University;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * /WSEcoResilenceNDP/rest/npdProgram/*
 * @author FerCho
 */
@Component
@Path("/npdProgram")
public class EcoResilenceServiceImpl implements IEcoResilenceService {
    
    private static final Logger log = Logger.getLogger(EcoResilenceServiceImpl.class);
    
    @Autowired
    private IEcoResilenceBO ecoResilenceBO;
    
    @POST
    @Path("/doLogin")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response doLogin(Login objLogin){
        ResultMessage result;
        
        if(null == objLogin ){
            
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Invalid input parameters");
            log.error(result);
            return Response.status(Status.NOT_FOUND).entity(result).build();
            
        }
        
        Detail detail = null;
        
        try {
            
            detail = ecoResilenceBO.doLogin(objLogin);
            
            ObjectMapper mapper = new ObjectMapper();
            log.info("detail:: " + mapper.writeValueAsString(detail));
            
        } catch (Exception e) {
            log.error("Error on login:: " + e);
            e.printStackTrace();
        }
        
        
        return Response.status(Status.OK).entity(detail).build();
    }

    @POST
    @Path("/registration")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response userRegistration(Person person) {
       
        ResultMessage result;
        
        if(null == person){
            
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Invalid input parameters");
            log.error(result);
            return Response.status(Status.NOT_FOUND).entity(result).build();
            
        }
        
        if( ecoResilenceBO.emailExist(person.getMail()) ){
            result = new ResultMessage();
            result.setStatusCode(Status.CONFLICT.getStatusCode());
            result.setMessage("The user already exists");
            log.info(result);
            return Response.status(Status.CONFLICT).entity(result).build();
        }
        
        
        Person registeredUser = ecoResilenceBO.register(person);
        
        if(registeredUser == null) {
            
            result = new ResultMessage();
            result.setStatusCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
            result.setMessage("An error ocured while creating the new user");
            log.error(result);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(result).build();
            
        }
        
        return Response.status(Status.OK).entity(registeredUser).build();
        
    }
    
    @GET
    @Path("/getSections/profile/{id_profile:. *}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response getSectionsByProfileId(@PathParam("id_profile") int id_profile){
        
        ResultMessage result = null;
        
        if(id_profile == 0){
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Invalid input parameters");
            log.error(result);
            return Response.status(Status.NOT_FOUND).entity(result).build();
            
        }
        
        
        List<Section> catSections = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        catSections = ecoResilenceBO.getCatSection(id_profile);
        try {
            String jsonSections = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(catSections);
            catSections = mapper.readValue(jsonSections, 
                    mapper.getTypeFactory().constructCollectionType(List.class, Section.class));
            log.info(catSections);
        } catch (JsonParseException e) {
            e.printStackTrace();
            log.error("Error when trying to parse catSections to Json:: --> " + e);
            
        }  catch (Exception e) {
            e.printStackTrace();
            log.error("Error while retriving sections:: --> " + e);
        }
        
        
        return Response.status(Status.OK).entity(catSections).build(); 
    }

    @GET
    @Path("/getCatStates")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response getCatStates() {
        ResultMessage result = null;
        
        List<State> states = ecoResilenceBO.getAllStates();
        
        
        if(states == null){
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Not states found");
            return Response.status(Status.NOT_FOUND).entity(result).build();
            
        }
        
        return Response.status(Status.OK).entity(states).build();
    }

    @GET
    @Path("/getCatDistricts")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response getCatDistricts() {
        ResultMessage result = null;
        
        List<District> districts = ecoResilenceBO.getAllDistricts();
        
        
        if(districts == null){
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Not districts found");
            return Response.status(Status.NOT_FOUND).entity(result).build();
            
        }
        
        return Response.status(Status.OK).entity(districts).build();
    }

    @GET
    @Path("/getCatUniversities")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response getCatUniversities() {
        ResultMessage result = null;
        
        List<University> universities = ecoResilenceBO.getAllUniversities();
        
        
        if(universities == null){
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Not unversities found");
            return Response.status(Status.NOT_FOUND).entity(result).build();
            
        }
        
        return Response.status(Status.OK).entity(universities).build();
    }

    @GET
    @Path("/getAppDetail")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Override
    public Response getAppDetail() {
        
        ResultMessage result = null;
        
        AppDetail appDetial = ecoResilenceBO.getAppDetail();
        
        if(appDetial == null){
            result = new ResultMessage();
            result.setStatusCode(Status.NOT_FOUND.getStatusCode());
            result.setMessage("Not app detail found");
            return Response.status(Status.NOT_FOUND).entity(result).build();
        }
        
        return Response.status(Status.OK).entity(appDetial).build();
    }
    
}


