package edu.eco.resilence.npd.service;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.apache.log4j.Logger;

public class EcoResilenceRestFilter implements ContainerRequestFilter {

    private static final Logger log = Logger.getLogger(EcoResilenceServiceImpl.class);
    private StringBuilder sb = null;

    private static final String PATH_LOGIN = "/WSEcoResilenceNPD/rest/npdProgram/doLogin";
    private static final String PATH_GET_SECTIONS = "/WSEcoResilenceNPD/rest/npdProgram/getSections/";
    
    private static final String JSON_MEDIA_TYPE = "application/json";

    public ContainerRequest filter(ContainerRequest request) {
        log.debug("Inicialización de OperacionesRestFilter");

        String jsonEntrada = "";
        InputStream entrada = null;
        InputStream stream = null;
        
        if(request.getMethod().equalsIgnoreCase("POST")){
            if (request.getMediaType().toString().trim().equals(JSON_MEDIA_TYPE)) {

                log.debug("Valor de getRequestUri().getPath: " + request.getRequestUri().getPath());

                log.debug("Valor de request.getMediaType(): " + request.getMediaType().toString());

                if ((request.getRequestUri().getPath().trim().equals(PATH_LOGIN))) {
                    entrada = request.getEntityInputStream();
                    log.debug("Media Type Json y login");

                    jsonEntrada = getStringFromInputStream(entrada);
                    log.debug("obtiene inputStream y lo transforma a String. jsonEntrada: " + jsonEntrada);

                    if (!"".equals(jsonEntrada)) {
                        log.debug("json de entrada contiene información");
                        log.debug("obtiene String de jsonEntrada y lo transforma a InputStream para devolverlo enla petición");
                        stream = new ByteArrayInputStream(jsonEntrada.getBytes(Charset.forName("UTF-8")));
                        request.setEntityInputStream(stream);

                    } else {

                        String jsonSalida = "{\"username\" : \"\",\"password\" : \"\",\"active\" : 0}";

                        log.debug("Parámetros de entrada incorrectos" + jsonSalida);
                        stream = new ByteArrayInputStream(jsonSalida.getBytes(Charset.forName("UTF-8")));
                        request.setEntityInputStream(stream);
                    }

                }

            } else {
                log.debug("dentro de else. No es una petición de operaciones");
            }
        } else {
            log.debug("Method:: " + request.getMethod());
        }

        return request;
    }
        

    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        sb = new StringBuilder();

        try {
            br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            return sb.toString();
        } catch (IOException e) {
            log.debug("Error al convertir el input stream en string");
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    log.debug("Error al cerrar el input stream");
                }
            }
        }
        return null;
    }
}
