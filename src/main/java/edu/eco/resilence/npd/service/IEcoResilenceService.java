package edu.eco.resilence.npd.service;

import edu.eco.resilence.npd.model.Login;
import edu.eco.resilence.npd.model.Person;
import javax.ws.rs.core.Response;

/**
 *
 * @author FerCho
 */
public interface IEcoResilenceService {
    
    public Response doLogin(Login objLogin);
    
    public Response userRegistration(Person person);
    
    public Response getSectionsByProfileId(int id_profile);
    
    public Response getCatStates();
    
    public Response getCatDistricts();
    
    public Response getCatUniversities();
    
    public Response getAppDetail();
    
}
