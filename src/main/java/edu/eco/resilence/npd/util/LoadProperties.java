/**
 * Utileria para cargar propiedades desde un archivo externo .properties
 */
package edu.eco.resilence.npd.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author marco.bernal
 *
 */
public class LoadProperties {

	private static final Logger log = Logger.getLogger(LoadProperties.class);	
	
	public static String getProperty(String property) {

		String propiedad = null;
		Properties props = null;
		InputStream is = null;
		
		try {
			
			props = new Properties();
			is = LoadProperties.class.getClassLoader()
                                .getResourceAsStream("ecoresiliencenpd.properties");
			props.load(is);      
			propiedad = props.getProperty(property);
			log.debug("Propiedad: " + propiedad + " leida correctamente");
			
		} catch (IOException e) {
			log.error("Archivo no encontrado -> \n", e);
		}
				
		return propiedad;
	}

}
