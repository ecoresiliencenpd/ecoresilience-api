package edu.eco.resilence.npd.util;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.log4j.Logger;

/**
 * @author marco.bernal
 *
 */
public class ConexionMail {
	
	private static final Logger log = Logger.getLogger(ConexionMail.class);
	
	private static ConexionMail singleton = null;
	private static Session session;
	
	/**
	 * Constructor privado para evitar instanciaci�n
	 */
	private ConexionMail() {
		
	}
		
	/**
	 * Obtiene instancia del Singleton
	 * @return
	 */
	public static ConexionMail getInstance() {
		
		// TODO Auto-generated method stub
		if(singleton == null) {
		   
			synchronized(ConexionMail.class) {
		    
				if(singleton == null) {
		        
					singleton = new ConexionMail();
					
					log.debug("Creando singleton nuevo.");
		        }
		    }
		}
		
		else {
			log.debug("Usando singleton actual.");
		}
		
	  return singleton;
	}
	
	/**
	 * Realiza la conexi�n al correo electr�nico
	 * @param user
	 * @param pass
	 * @return
	 */
	protected static Session obtieneSesion(String user, String pass) {
		
		final String username = user;
		final String password = pass;
		
		/*
		 * Carga Propiedades
		 * */
		Properties props = new Properties();
                props.put("mail.transport.protocol", "smtp");
		/*props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", LoadProperties.getProperty("smtp.host"));
		props.put("mail.smtp.port", LoadProperties.getProperty("smtp.port"));*/
                
                //mail without authentication
                props.put("mail.smtp.auth", "false");
                //Put below to false, if no https is needed
                props.put("mail.smtp.starttls.enable", "false");
                props.put("mail.smtp.host", LoadProperties.getProperty("smtp.host"));
                props.put("mail.smtp.port", LoadProperties.getProperty("smtp.port"));

                session = Session.getInstance(props);
		
		log.debug("Propiedades asignadas correctamente.");
		
		/*
		 * Conexion
		 * */
		
		session = null;
		
		session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		
		log.debug("Objeto de Session creado correctamente.");
		return session;
		
	}
	
}
