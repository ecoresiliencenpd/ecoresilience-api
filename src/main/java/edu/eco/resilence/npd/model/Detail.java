package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class Detail implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Person person;
    private Catalog catalog;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    @Override
    public String toString() {
        return "Detail{" + "person=" + person + ", catalog=" + catalog + '}';
    }
    
}
