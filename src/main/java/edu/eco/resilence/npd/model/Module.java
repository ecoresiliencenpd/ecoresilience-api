package edu.eco.resilence.npd.model;

import java.util.List;

/**
 *
 * @author FerCho
 */
public class Module {
    
    private int id_title_module;
    private String title_module;
    private List<Content> contents;

    public int getId_title_module() {
        return id_title_module;
    }

    public void setId_title_module(int id_title_module) {
        this.id_title_module = id_title_module;
    }

    public String getTitle_module() {
        return title_module;
    }

    public void setTitle_module(String title_module) {
        this.title_module = title_module;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "Module2{" + "id_title_module=" + id_title_module + ", title_module=" + title_module + ", contents=" + contents + '}';
    }
    
}
