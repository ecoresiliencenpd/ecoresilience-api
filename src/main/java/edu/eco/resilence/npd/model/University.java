package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class University implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id_university;
    private String university_name;
    private int active;
    private int id_state;
    private int id_district;

    public int getId_university() {
        return id_university;
    }

    public void setId_university(int id_university) {
        this.id_university = id_university;
    }

    public String getUniversity_name() {
        return university_name;
    }

    public void setUniversity_name(String university_name) {
        this.university_name = university_name;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getId_state() {
        return id_state;
    }

    public void setId_state(int id_state) {
        this.id_state = id_state;
    }

    public int getId_district() {
        return id_district;
    }

    public void setId_district(int id_district) {
        this.id_district = id_district;
    }

    @Override
    public String toString() {
        return "University{" + "id_university=" + id_university + ", university_name=" + university_name + ", active=" + active + ", id_state=" + id_state + ", id_district=" + id_district + '}';
    }
    
}
