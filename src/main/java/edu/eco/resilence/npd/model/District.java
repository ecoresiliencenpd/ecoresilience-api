package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class District implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id_district;
    private String district_name;
    private int active;
    private int id_state;

    public int getId_district() {
        return id_district;
    }

    public void setId_district(int id_district) {
        this.id_district = id_district;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getId_state() {
        return id_state;
    }

    public void setId_state(int id_state) {
        this.id_state = id_state;
    }

    @Override
    public String toString() {
        return "District{" + "id_district=" + id_district + ", district_name=" + district_name + ", active=" + active + ", id_state=" + id_state + '}';
    }
    
}
