package edu.eco.resilence.npd.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author FerCho
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Login implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String username;
    private String password;
    private int active;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Login{" + "username=" + username + ", password=" + password + ", active=" + active + '}';
    }
    
    
}
