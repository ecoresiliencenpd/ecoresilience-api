package edu.eco.resilence.npd.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author FerCho
 */
public class AppDetail implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private AppVersion version;
    private AppContactSupport contactSupport;
    private AppPolicy appPolicy;
    private List<AppSocialMedia> socialMedia;

    public AppVersion getVersion() {
        return version;
    }

    public void setVersion(AppVersion version) {
        this.version = version;
    }

    public AppContactSupport getContactSupport() {
        return contactSupport;
    }

    public void setContactSupport(AppContactSupport contactSupport) {
        this.contactSupport = contactSupport;
    }

    public AppPolicy getAppPolicy() {
        return appPolicy;
    }

    public void setAppPolicy(AppPolicy appPolicy) {
        this.appPolicy = appPolicy;
    }

    public List<AppSocialMedia> getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(List<AppSocialMedia> socialMedia) {
        this.socialMedia = socialMedia;
    }
    
    
}
