package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class State implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id_state;
    private String state_name;
    private int active;

    public int getId_state() {
        return id_state;
    }

    public void setId_state(int id_state) {
        this.id_state = id_state;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "State{" + "id_state=" + id_state + ", state_name=" + state_name + ", active=" + active + '}';
    }
    
}
