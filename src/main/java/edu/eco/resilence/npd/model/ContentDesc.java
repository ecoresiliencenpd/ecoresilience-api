package edu.eco.resilence.npd.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author FerCho
 */
public class ContentDesc implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int id_content;
    private String content_desc;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    private Date last_update;
    private String url_image;
    private String url_doc;

    public int getId_content() {
        return id_content;
    }

    public void setId_content(int id_content) {
        this.id_content = id_content;
    }

    public String getContent_desc() {
        return content_desc;
    }

    public void setContent_desc(String content_desc) {
        this.content_desc = content_desc;
    }

    public Date getLast_update() {
        return last_update;
    }

    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getUrl_doc() {
        return url_doc;
    }

    public void setUrl_doc(String url_doc) {
        this.url_doc = url_doc;
    }

    @Override
    public String toString() {
        return "ContentDesc{" + "id_content=" + id_content + ", content_desc=" + content_desc + ", last_update=" + last_update + '}';
    }
    
}
