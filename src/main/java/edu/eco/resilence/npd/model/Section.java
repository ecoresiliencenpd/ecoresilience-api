package edu.eco.resilence.npd.model;


import java.io.Serializable;
import java.util.List;

/**
 *
 * @author FerCho
 */
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Section implements Serializable {
    
    private static final long serialVersionUID = 1L; 
    
    private int id_section;
    private String section_name;
    private List<Module> modules;

    public int getId_section() {
        return id_section;
    }

    public void setId_section(int id_section) {
        this.id_section = id_section;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }
    
    public List<Module> getModules() {
        return modules;
    }
    
    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    @Override
    public String toString() {
        return "Sections{" + "id_section=" + id_section + ", section_name=" + section_name + ", modules=" + modules + '}';
    }
    
}
