package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int id_person;
    private String mail;
    private String password;
    private String first_name;
    private String last_name;
    private String middle_name;
    private int id_profile;
    private String profile_name;
    private int id_state;
    private String state_name;
    private int id_district;
    private String district_name;
    private int id_university;
    private String university_name;
    private int user_registered;
    private int id_user;
    private User user;

    public int getId_person() {
        return id_person;
    }

    public void setId_person(int id_person) {
        this.id_person = id_person;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public int getId_profile() {
        return id_profile;
    }

    public void setId_profile(int id_profile) {
        this.id_profile = id_profile;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public int getId_state() {
        return id_state;
    }

    public void setId_state(int id_state) {
        this.id_state = id_state;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public int getId_district() {
        return id_district;
    }

    public void setId_district(int id_district) {
        this.id_district = id_district;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public int getId_university() {
        return id_university;
    }

    public void setId_university(int id_university) {
        this.id_university = id_university;
    }

    public String getUniversity_name() {
        return university_name;
    }

    public void setUniversity_name(String university_name) {
        this.university_name = university_name;
    }

    public int getUser_registered() {
        return user_registered;
    }

    public void setUser_registered(int user_registered) {
        this.user_registered = user_registered;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Person{" + "id_person=" + id_person + ", mail=" + mail + ", first_name=" + first_name + ", last_name=" + last_name + ", middle_name=" + middle_name + ", id_profile=" + id_profile + ", profile_name=" + profile_name + ", id_state=" + id_state + ", state_name=" + state_name + ", id_district=" + id_district + ", district_name=" + district_name + ", id_university=" + id_university + ", university_name=" + university_name + ", user_registered=" + user_registered + ", id_user=" + id_user + ", user=" + user + '}';
    }
    
}
