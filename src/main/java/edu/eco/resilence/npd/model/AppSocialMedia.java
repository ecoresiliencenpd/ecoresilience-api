package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class AppSocialMedia implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String name;
    private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
}
