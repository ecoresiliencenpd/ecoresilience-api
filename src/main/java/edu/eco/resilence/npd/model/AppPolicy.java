/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class AppPolicy implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String name;
    private String link;

    public String getName() {
        return name;
    }

    public void setPolicyName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
