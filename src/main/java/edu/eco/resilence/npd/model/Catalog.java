package edu.eco.resilence.npd.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author FerCho
 */
public class Catalog implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int id_profile;
    private String profile_name;
    private List<Section> sections;

    public int getId_profile() {
        return id_profile;
    }

    public void setId_profile(int id_profile) {
        this.id_profile = id_profile;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    @Override
    public String toString() {
        return "Catalog2{" + "id_profile=" + id_profile + ", profile_name=" + profile_name + ", sections=" + sections + '}';
    }
    
}
