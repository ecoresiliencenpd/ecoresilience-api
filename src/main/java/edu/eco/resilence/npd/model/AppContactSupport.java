package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class AppContactSupport implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String name;
    private String mail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setContactSupportMail(String mail) {
        this.mail = mail;
    }
    
}
