package edu.eco.resilence.npd.model;

import java.util.List;

/**
 *
 * @author FerCho
 */
public class Content {
    
    private int id_title_content;
    private String title_content;
    private List<ContentDesc> articles;

    public int getId_title_content() {
        return id_title_content;
    }

    public void setId_title_content(int id_title_content) {
        this.id_title_content = id_title_content;
    }

    public String getTitle_content() {
        return title_content;
    }

    public void setTitle_content(String title_content) {
        this.title_content = title_content;
    }

    public List<ContentDesc> getArticles() {
        return articles;
    }

    public void setArticles(List<ContentDesc> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Content2{" + "id_title_content=" + id_title_content + ", title_content=" + title_content + ", articles=" + articles + '}';
    }
    
}
