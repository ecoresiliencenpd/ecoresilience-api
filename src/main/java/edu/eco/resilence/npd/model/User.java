package edu.eco.resilence.npd.model;

import java.io.Serializable;

/**
 *
 * @author FerCho
 */
public class User implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private int id_user;
    private String username;
    private String password;
    private int id_profile;
    private int active;
    private String token;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId_profile() {
        return id_profile;
    }

    public void setId_profile(int id_profile) {
        this.id_profile = id_profile;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "User{" + "id_user=" + id_user + ", username=" + username + ", password=" + password + ", id_profile=" + id_profile + ", active=" + active + ", token=" + token + '}';
    }
    
}
