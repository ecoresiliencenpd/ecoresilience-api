package edu.eco.resilence.npd.bo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import edu.eco.resilence.npd.dao.IEcoResilenceDAO;
import edu.eco.resilence.npd.model.AppDetail;
import edu.eco.resilence.npd.model.Login;
import edu.eco.resilence.npd.model.Person;
import edu.eco.resilence.npd.model.Section;
import edu.eco.resilence.npd.model.User;
import edu.eco.resilence.npd.model.Catalog;
import edu.eco.resilence.npd.model.Detail;
import edu.eco.resilence.npd.model.District;
import edu.eco.resilence.npd.model.State;
import edu.eco.resilence.npd.model.University;
import edu.eco.resilence.npd.util.LoadProperties;
import java.util.List;
import java.util.UUID;
import org.apache.log4j.Logger;

/**
 *
 * @author FerCho
 */
public class EcoResilenceBO implements IEcoResilenceBO {

    private static final Logger log = Logger.getLogger(EcoResilenceBO.class);

    private IEcoResilenceDAO ecoResilenceDAO;

    @Override
    public Detail doLogin(Login login) {

        Detail catalogDetail = null;

        log.info("doLogin2...");

        Person person = null;
        Catalog catalog = null;
        try {

            login.setActive(1);
            log.info("Doing login2:: ->\n" + login);
            person = ecoResilenceDAO.obtieneDatosLogin(login);

            log.info("Person recovery data:: ->\n" + person + "\n\n");

            catalog = ecoResilenceDAO.getCatalogByProfileId(person.getId_profile());
            
            ObjectMapper mapper = new ObjectMapper();

            catalogDetail = new Detail();
            catalogDetail.setPerson(person);
            catalogDetail.setCatalog(catalog);       
            
            mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

            log.info("Data revocered successfully");

        } catch (Exception ex) {
            log.error("Error when getting user's data:: -> " + ex);
            ex.printStackTrace();

        }

        return catalogDetail;

    }
    
    @Override
    public boolean emailExist(String email) {
        User user = ecoResilenceDAO.findByEmail(email);
        return user != null;
    }

    @Override
    public Person register(Person person) {

        User user = null;
        log.info("Registration of new user/person:: -->\n" + person);

        try {

            user = new User();
            user.setUsername(person.getMail());
            user.setPassword(person.getPassword());
            user.setId_profile(person.getId_profile());
            user.setToken(UUID.randomUUID().toString());

            ecoResilenceDAO.createUser(user);
            person.setId_user(user.getId_user());
            ecoResilenceDAO.registerUser(person);
            person.setUser(user);
            log.info("Person Registered successfully:: -->\n\t" + person);

        } catch (Exception e) {
            log.error("Error while creating the new user:: --> " + e);
            e.printStackTrace();
        }

        return person;

    }

    @Override
    public List<Section> getCatSection(int id_profile) {

        List<Section> sections = null;

        log.info("Getting the sections by user's profile....");

        final int active = Integer.valueOf(LoadProperties.getProperty("db.active"));

        try {

            sections = ecoResilenceDAO.getSectionsByProfile(id_profile, active);

            log.info("Sections recovered successfully:: -->\n" + sections);

        } catch (Exception e) {
            log.error("Error on getting catSections: " + e);
            e.printStackTrace();
        }

        return sections;
    }

    @Override
    public List<State> getAllStates() {
        List<State> states = null;
        
        log.info("Getting cat of states...");
        
        Integer active = Integer.valueOf(LoadProperties.getProperty("db.active"));
        
        try {
            states = ecoResilenceDAO.getAllStates(active);
        } catch (Exception e) {
            log.error("Error on getting states: " + e);
            e.printStackTrace();
        }
        
        return states;
    }

    @Override
    public List<District> getAllDistricts() {
        List<District> districts = null;
        
        log.info("Getting cat of districts...");
        
        Integer active = Integer.valueOf(LoadProperties.getProperty("db.active"));
        
        try {
            districts = ecoResilenceDAO.getAllDistricts(active);
        } catch (Exception e) {
            log.error("Error on getting districts: " + e);
            e.printStackTrace();
        }
        
        return districts;
    }

    @Override
    public List<University> getAllUniversities() {
        List<University> universities = null;
        
        log.info("Getting cat of universities...");
        
        Integer active = Integer.valueOf(LoadProperties.getProperty("db.active"));
        
        try {
            universities = ecoResilenceDAO.getAllUniversities(active);
        } catch (Exception e) {
            log.error("Error on getting universities: " + e);
            e.printStackTrace();
        }
        
        return universities;
    }
    
    @Override
    public AppDetail getAppDetail() {
        AppDetail appDetail = null;
        
        log.info("Getting app detail...");
        
        Integer active = Integer.valueOf(LoadProperties.getProperty("db.active"));
        
        try {
            appDetail = ecoResilenceDAO.getAppDetail(active);
        } catch (Exception e) {
            log.error("Error on getting app detail: " + e);
            e.printStackTrace();
        }
        
        return appDetail;
    }
    
    /**
     * DI
     *
     * @param ecoResilenceDAO
     */
    public void setEcoResilenceDAO(IEcoResilenceDAO ecoResilenceDAO) {
        this.ecoResilenceDAO = ecoResilenceDAO;
    }

}
