package edu.eco.resilence.npd.bo;

import edu.eco.resilence.npd.model.AppDetail;
import edu.eco.resilence.npd.model.Login;
import edu.eco.resilence.npd.model.Person;
import edu.eco.resilence.npd.model.Section;
import edu.eco.resilence.npd.model.Detail;
import edu.eco.resilence.npd.model.District;
import edu.eco.resilence.npd.model.State;
import edu.eco.resilence.npd.model.University;
import java.util.List;

/**
 *
 * @author FerCho
 */
public interface IEcoResilenceBO {
    
    public Detail doLogin(Login login);

    public boolean emailExist(String email);
    
    public Person register(Person person);
    
    public List<Section> getCatSection(int id_profile);
    
    public List<State> getAllStates();
    
    public List<District> getAllDistricts();
    
    public List<University> getAllUniversities();
    
    public AppDetail getAppDetail();
    
}
