package edu.eco.resilence.npd.dao;

import edu.eco.resilence.npd.model.AppDetail;
import edu.eco.resilence.npd.model.Login;
import edu.eco.resilence.npd.model.Person;
import edu.eco.resilence.npd.model.Section;
import edu.eco.resilence.npd.model.User;
import edu.eco.resilence.npd.model.Catalog;
import edu.eco.resilence.npd.model.District;
import edu.eco.resilence.npd.model.State;
import edu.eco.resilence.npd.model.University;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author FerCho
 */
public interface IEcoResilenceDAO {
    
    public Person obtieneDatosLogin(Login login);
    
    public List<Section> getSectionsByProfile(@Param("id_profile") int id_profile, @Param("active") int active);
    
    public Catalog getCatalogByProfileId(@Param("id_profile") int id_profile);
    
    public User findByEmail(@Param("email") String email);
    public void createUser(User user);
    public void registerUser(Person person);
    
    public List<State> getAllStates(@Param("active") int active);
    public List<District> getAllDistricts(@Param("active") int active);
    public List<University> getAllUniversities(@Param("active") int active);
    
    public AppDetail getAppDetail(@Param("active") int active);
    
}
