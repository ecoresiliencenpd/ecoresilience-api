# README #

REST Services for the App Eco-Resilience NPD Program

### What is this repository for? ###

* This repo contains the full project REST Services for the App Eco-Resilience NPD Program

### How do I get set up? ###

* Java version: 1.8.0_45, vendor: Oracle Corporation
* Tomcat 8.5 (I will confirm this)
* Apache Maven 3.3.3

### Contribution guidelines ###

* Writing tests:	Fernando Fernández
* Code review		Marco Bernal (if he want's)
* Other stuff... 	TBD	

### Who do I talk to? ###

* Fernando Fernández
* fer@qcode.mx